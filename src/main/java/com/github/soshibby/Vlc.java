package com.github.soshibby;

import com.github.soshibby.client.VLCClient;
import com.github.soshibby.exceptions.VLCConnectionException;
import com.github.soshibby.exceptions.VLCResponseException;
import com.github.soshibby.types.Status;
import org.conductor.component.annotations.ComponentOption;
import org.conductor.component.annotations.Property;
import org.conductor.component.annotations.ReadOnlyProperties;
import org.conductor.component.annotations.ReadOnlyProperty;
import org.conductor.component.types.Component;
import org.conductor.component.types.DataType;
import org.conductor.database.Database;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.ConnectException;
import java.util.Map;

@org.conductor.component.annotations.Component(type = "vlc")
@ComponentOption(dataType = "string", name = "host", defaultValue = "127.0.0.1")
@ComponentOption(dataType = "integer", name = "port", defaultValue = "8080")
@ComponentOption(dataType = "string", name = "username", defaultValue = "")
@ComponentOption(dataType = "string", name = "password", defaultValue = "password")
@ReadOnlyProperty(name = "length", dataType = DataType.INTEGER, defaultValue = "0")
public class Vlc extends Component implements Runnable {

    private static final Logger log = LoggerFactory.getLogger(Vlc.class);
    private Thread vlcStatusUpdateThread = null;
    private VLCClient client;
    private int MAX_VOLUME = 256;

    public Vlc(Database database, Map<String, Object> options) {
        super(database, options);

        client = new VLCClient(
                (String) options.get("host"),
                (Integer) options.get("port"),
                (String) options.get("username"),
                (String) options.get("password")
        );

        vlcStatusUpdateThread = new Thread(this);
        vlcStatusUpdateThread.setDaemon(true);
        vlcStatusUpdateThread.start();
    }

    @Property(initialValue = "false")
    public void setPlaying(Boolean play) throws VLCResponseException, VLCConnectionException {
        if (play) {
            client.play();
        } else {
            client.pause();
        }
    }

    @Property(initialValue = "0")
    public void setVolume(Integer volumePercentage) throws VLCResponseException, VLCConnectionException {
        client.setVolume(volumePercentage);
    }

    @Property(initialValue = "0")
    public void setTime(Integer time) throws VLCResponseException, VLCConnectionException {
        client.setTime(time);
    }

    @Property(initialValue = "false")
    public void setFullscreen(Boolean fullscreen) throws Exception {
        Boolean isCurrentlyFullscreen = (Boolean) getPropertyValue("fullscreen");

        if (isCurrentlyFullscreen != fullscreen) {
            client.toggleFullscreen();
        }
    }

    private void updateVLCStatus() throws Exception {
        Status status = client.getStatus();

        boolean isPlaying = status.getState().equals("playing");
        updatePropertyValue("playing", isPlaying);
        updatePropertyValue("fullscreen", status.isFullscreen());
        // updatePropertyValue("time", status.getTime());
        updatePropertyValue("length", status.getLength());

        int volumePercentage = (int) (((double) status.getVolume() / (double) MAX_VOLUME) * 100);
        updatePropertyValue("volume", volumePercentage);
    }

    public void run() {
        try {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    updateVLCStatus();
                } catch (VLCConnectionException e) {
                    try {
                        updatePropertyValue("playing", false);
                    } catch (Exception e1) {
                        log.error("Failed to save vlc status to database.", e);
                    }
                    log.warn("{}", e);
                } catch (Throwable e) {
                    log.error("Error when retrieving vlc status.", e);
                }
                Thread.sleep(400);
            }
        } catch (InterruptedException e) {

        }

        log.warn("Stopped receiving status messages.");
    }

    @Override
    public void destroy() {
        vlcStatusUpdateThread.interrupt();
    }
}
