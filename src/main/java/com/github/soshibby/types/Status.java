package com.github.soshibby.types;

import com.google.gson.annotations.SerializedName;

public class Status {
	@SerializedName("fullscreen")
	private boolean fullscreen;
	
	@SerializedName("state")
	private String state;
	
	@SerializedName("time")
	private int time;
	
	@SerializedName("length")
	private int length;
	
	@SerializedName("volume")
	private int volume;
	
	public String getState(){
		return state;
	}
	
	public boolean isFullscreen(){
		return fullscreen;
	}
	
	public int getTime(){
		return time;
	}
	
	public int getLength(){
		return length;
	}
	
	public int getVolume(){
		return volume;
	}
}
