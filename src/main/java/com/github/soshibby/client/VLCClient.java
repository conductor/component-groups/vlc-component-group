package com.github.soshibby.client;

import com.github.soshibby.types.Status;
import com.github.soshibby.adapters.BooleanAsIntAdapter;
import com.github.soshibby.exceptions.VLCConnectionException;
import com.github.soshibby.exceptions.VLCResponseException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import org.apache.cxf.helpers.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Henrik on 04/12/2016.
 */
public class VLCClient {

    private String url;
    private String username;
    private String password;
    private int MAX_VOLUME = 256;
    private BooleanAsIntAdapter booleanAsIntAdapter = new BooleanAsIntAdapter();
    Gson gson = new GsonBuilder().registerTypeAdapter(Boolean.class, booleanAsIntAdapter).registerTypeAdapter(boolean.class, booleanAsIntAdapter).create();

    public VLCClient(String host, int port, String username, String password) {
        this.url = "http://" + host + ":" + port;
        this.username = username;
        this.password = password;
    }

    public void play() throws VLCResponseException, VLCConnectionException {
        sendCommand("command=pl_play");
    }

    public void pause() throws VLCResponseException, VLCConnectionException {
        sendCommand("command=pl_forcepause");
    }

    public void setVolume(int percentage) throws VLCResponseException, VLCConnectionException {
        double realVolume = (double) MAX_VOLUME * ((double) percentage / 100);
        sendCommand("command=volume&val=" + realVolume);
    }

    public void setTime(int time) throws VLCResponseException, VLCConnectionException {
        sendCommand("command=seek&val=" + String.valueOf(time));
    }

    public void toggleFullscreen() throws VLCResponseException, VLCConnectionException {
        sendCommand("command=fullscreen");
    }

    public Status getStatus() throws VLCResponseException, VLCConnectionException {
        CloseableHttpClient client = createWebClient();

        try {
            try {
                String response = sendCommand("");
                return gson.fromJson(response, Status.class);
            } finally {
                client.getConnectionManager().shutdown();
            }
        } catch (JsonSyntaxException | JsonIOException e) {
            throw new VLCResponseException("Failed to convert vlc json response.", e);
        }
    }

    private String sendCommand(String command) throws VLCResponseException, VLCConnectionException {
        try (CloseableHttpClient client = createWebClient()) {
            String url = String.format("%s/requests/status.json?%s", this.url, command);
            HttpGet request = new HttpGet(url);
            HttpResponse response = client.execute(request);

            int statusCode = response.getStatusLine().getStatusCode();

            if (statusCode != 200 && statusCode != 204) {
                throw new VLCResponseException("Received status code '" + statusCode + "' from vlc for request '" + command + "'. " + response.getEntity().toString());
            }
            try {
                return IOUtils.toString(response.getEntity().getContent());
            } catch (IOException e) {
                throw new VLCResponseException("Failed to read vlc response.", e);
            }
        } catch (IOException e) {
            throw new VLCConnectionException("Failed to connect to vlc.", e);
        }
    }

    private CloseableHttpClient createWebClient() {
        CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(this.username, this.password));
        return HttpClientBuilder.create().setDefaultCredentialsProvider(credentialsProvider).build();
    }

}
