package com.github.soshibby.exceptions;

import java.io.IOException;

/**
 * Created by Henrik on 04/12/2016.
 */
public class VLCConnectionException extends Exception {

    public VLCConnectionException(String message) {
        super(message);
    }

    public VLCConnectionException(String message, Throwable e) {
        super(message, e);
    }

}
