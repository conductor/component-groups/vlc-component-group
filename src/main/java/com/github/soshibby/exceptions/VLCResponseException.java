package com.github.soshibby.exceptions;

/**
 * Created by Henrik on 04/12/2016.
 */
public class VLCResponseException extends Exception {

    public VLCResponseException(String message) {
        super(message);
    }

    public VLCResponseException(String message, Throwable e) {
        super(message, e);
    }

}
