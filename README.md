# VLC Component Group For Conductor
Component group that is used for controlling VLC with Conductor.

## Features
- Play, pause and change volume, fullscreen state and seek time.
- Receive current state of the VLC player such as total media length, volume, fullscreen state and playing state.